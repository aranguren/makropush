package com.deneb.makropush.model;

import com.deneb.xmppservice.Connection;
import com.deneb.xmppservice.XmppListener;

public class PushService {

	private static PushService instance;
	
	public static PushService getInstance(){
		if(instance == null)
			instance = new PushService();
		
		return instance;
	}
	
	public interface OnAuthListener{	
		void auth(boolean success, String errorDetails);
	}
	
	public interface OnMessageListener {
		void onMessage(String from, String body);
	}
	
	private class PushServiceListener implements XmppListener {
		
		public void onUnsubscribeEvent(String jid) {
			// TODO Auto-generated method stub
			
		}
		
		public void onSubscribeEvent(String jid) {
			// TODO Auto-generated method stub
			
		}
		
		public void onStatusEvent(String jid, String show, String status) {
			// TODO Auto-generated method stub
			
		}
		
		public void onRosterVCardEvent(String jid, String base64StringValue) {
			// TODO Auto-generated method stub
			
		}
		
		public void onMessageEvent(String from, String to, String body, String id) {
			currentMessageListener.onMessage(from, body);
		}
		
		public void onContactOverEvent() {
			// TODO Auto-generated method stub
			
		}
		
		public void onContactEvent(String jid, String name, String group,
				String subscription) {
			// TODO Auto-generated method stub
			
		}
		
		public void onConnFailed(String msg) {
			currentAuthListener.auth(false, "La conexion al servidor ha fallado. Intente luego.");
			currentMessageListener.onMessage("Push Makro", "La conexion con el servidor ha fallado. Intente Luego.");
		}
		
		public void onAuthFailed(String message) {
			currentAuthListener.auth(false, "Las credenciales son invalidas " + message);
			currentMessageListener.onMessage("Push Makro", "La autenticacion ha fallado.");

		}
		
		public void onAuth(String resource) {
			currentAuthListener.auth(true, "");
			currentMessageListener.onMessage("Push Makro", "Has establecido la conexion con el servidor");
		}
	};
	
	private OnAuthListener currentAuthListener = new OnAuthListener() {
		
		public void auth(boolean success, String errorDetails) {
		}
	};
	
	private OnMessageListener currentMessageListener = new OnMessageListener() {
		
		public void onMessage(String from, String body) {
			// TODO Auto-generated method stub
			
		}
	};
	
	private PushServiceListener pushServiceListener = new PushServiceListener();
	
	private PushService(){
		
	}
	
	public void connect(User user, OnMessageListener onMessageListener){
		Connection conn = Connection.getInstance();
		fillFromUser(Connection.getInstance(), user);
		
		if(onMessageListener != null){
			this.currentMessageListener = onMessageListener;
		}
		
		if(conn.getThread() == null || !conn.getThread().isAlive()){
			conn.connect(false);
			conn.addListener(pushServiceListener);
		}
	}
		
	public void auth(User user, OnAuthListener onAuthListener){
		Connection conn = Connection.getInstance();
		fillFromUser(conn, user);
		
		if(onAuthListener != null)
			currentAuthListener = onAuthListener;
		
		conn.connect(false);
		conn.addListener(pushServiceListener);
	}
	
	private void fillFromUser(Connection conn, User user){
		conn.setServer(user.getServerIp());
		conn.setUsername(user.getUsername());
		conn.setPassword(user.getPassword());
		conn.setPort(user.getPort());
		conn.setNetworkType(0);
		conn.setHost(user.getXmppServername());
	}
}
