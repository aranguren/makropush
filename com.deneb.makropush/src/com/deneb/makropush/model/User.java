package com.deneb.makropush.model;

import java.util.Hashtable;

import me.regexp.RE;
import net.rim.device.api.system.PersistentObject;
import net.rim.device.api.system.PersistentStore;

public class User {

	private static User instance;
	private PersistentObject persistence;
	
	private Hashtable userProperties;
	
	public static User getInstance(){
		if(instance == null)
			instance = new User();
		
		return instance;
	}
	
	private User(){
		persistence = PersistentStore.getPersistentObject(123123413412312312L);
		
		synchronized (persistence) {
			userProperties = (Hashtable)persistence.getContents();
		}
		
		if(userProperties == null){
			userProperties = new Hashtable();
		}
	}
	
	public void clean(){
		userProperties = new Hashtable();
		this.save();
	}
	
	public boolean isLogged(){
		return !userProperties.isEmpty();
	}
	
	public void save(){
		synchronized (persistence) {
			persistence.setContents(userProperties);
			persistence.commit();
		}
	}

	public String getServerIp() {
		return userProperties.get("serverIp").toString();
	}

	public void setServerIp(String serverIp) {
		userProperties.put("serverIp", serverIp);
	}

	public String getPort() {
		return userProperties.get("port").toString();
	}

	public void setPort(String port) {
		if(port.length() == 0)
			throw new IllegalArgumentException("Debes introducir el puerto");
		
		try{
			Integer.parseInt(port);		
		}catch(NumberFormatException e){
			throw new IllegalArgumentException("Debe ser un numero");
		}
		
		userProperties.put("port", port);
	}

	public String getUsername() {
		return userProperties.get("username").toString();
	}

	public void setUsername(String username) {
		if(username.length() == 0)
			throw new IllegalArgumentException("Debes introducir un usuario\n");
		
		String pattern = "[\\w\\d_\\-]+@[\\w\\d_\\-]+";
		RE regex = new RE(pattern);
		
		//if(!regex.match(username))
		//	throw new IllegalArgumentException("El usuario debe tener la estructura usuario@servidor");
		
		char[] usernameCharset = username.toCharArray();
		StringBuffer usernamePart = new StringBuffer();
		StringBuffer hostPart = new StringBuffer();
		boolean isHost = false;
		
		for(int i = 0; i < usernameCharset.length; i++){
			if(usernameCharset[i] == '@'){
				isHost = true;
				continue;
			}
			
			if(isHost)
				hostPart.append(usernameCharset[i]);
			else
				usernamePart.append(usernameCharset[i]);
		}
		
		userProperties.put("username", usernamePart.toString());
		setXmppServername(hostPart.toString());
	}

	public String getPassword() {
		return userProperties.get("password").toString();
	}

	public void setPassword(String password) {
		if(password.length() == 0)
			throw new IllegalArgumentException("Debes introducir una contrasena");
		
		userProperties.put("password", password);
	}

	public String getXmppServername() {
		return userProperties.get("xmppServer").toString();
	}

	public void setXmppServername(String xmppServername) {
		//if(xmppServername.length() == 0)
		//	throw new IllegalArgumentException("Debes introducir el nombre del servidor");
		
		userProperties.put("xmppServer", "");
	}

	public String toString() {
		return "User [userProperties=" + userProperties + "]";
	}
}
