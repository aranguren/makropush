package com.deneb.makropush;

import com.deneb.makropush.model.PushService;
import com.deneb.makropush.model.PushService.OnAuthListener;
import com.deneb.makropush.model.User;

import net.rim.device.api.system.Application;
import net.rim.device.api.ui.Color;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.Graphics;
import net.rim.device.api.ui.MenuItem;
import net.rim.device.api.ui.component.BasicEditField;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.PasswordEditField;
import net.rim.device.api.ui.component.SeparatorField;
import net.rim.device.api.ui.container.HorizontalFieldManager;
import net.rim.device.api.ui.container.MainScreen;

public class LoginScreen extends MainScreen {
	
	private BasicEditField txtUsername;
	private BasicEditField txtPassword;
	private BasicEditField txtServerIp;
	private BasicEditField txtPort;
	private MenuItem connectMenu = new MenuItem("Conectar", 110, 10)
	{
		public void run(){
			try {
				final User user = User.getInstance();
				user.setUsername(txtUsername.getText());
				user.setPassword(txtPassword.getText());
				user.setServerIp(txtServerIp.getText());
				user.setPort(txtPort.getText());
				PushService.getInstance().auth(user, new OnAuthListener() {
					
					public void auth(final boolean success, final String errorDetails) {
						final MakroPush app = (MakroPush)Application.getApplication();
						Application.getApplication().invokeLater(new Runnable() {
							public void run() {
								if(success){
									user.save();
									app.goToMain();
								}else{		
									errorMessage.setText(errorDetails);
									Dialog.alert("Login failed for " + user.toString());
								}								
							}
						});
					}
				});
				
			}catch(Exception e){
				errorMessage.setText(e.getMessage());
			}
		}
	};
	
	private LabelField errorMessage = new LabelField(null, LabelField.FIELD_HCENTER)
	{	
		public void paint(Graphics g){
			g.setColor(Color.RED);
			super.paint(g);
		}
	};
	
	public LoginScreen(){
		setTitle("Makro Push - Login");
		
		txtUsername = new BasicEditField(); 
		txtUsername.setNonSpellCheckable(true);

		txtPassword = new PasswordEditField();
		txtServerIp = new BasicEditField();
		txtPort = new BasicEditField();		
		
		addMenuItem(connectMenu);

		LabelField login = new LabelField("Acceder al sistema de mensajeria", LabelField.FIELD_HCENTER);
		login.setFont(Font.getDefault().derive(Font.BOLD, 30));
		login.setMargin(10, 0, 20, 0); //To leave some space from top and bottom
		add(login);
		add(errorMessage);
		
		HorizontalFieldManager[] fieldsets = new HorizontalFieldManager[4];
				
		for(int i = 0; i < fieldsets.length; i++){
			fieldsets[i] = new HorizontalFieldManager();
			fieldsets[i].setMargin(10, 0, 10, 5);
		}
		
		fieldsets[0].add(new LabelField("Usuario: "));
		fieldsets[0].add(txtUsername);
		
		fieldsets[1].add(new LabelField("Clave: "));
		fieldsets[1].add(txtPassword);
		
		fieldsets[2].add(new LabelField("IP Servidor: "));
		fieldsets[2].add(txtServerIp);
		
		fieldsets[3].add(new LabelField("Puerto: "));
		fieldsets[3].add(txtPort);
		
		for(int i = 0; i < fieldsets.length; i++){
			add(fieldsets[i]);
			if(i < fieldsets.length - 1)
				add(new SeparatorField());
		}
	}
}
