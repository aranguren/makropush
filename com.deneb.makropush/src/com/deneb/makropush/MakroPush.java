package com.deneb.makropush;

import com.deneb.makropush.model.User;

import net.rim.device.api.ui.UiApplication;

/**
 * This class extends the UiApplication class, providing a
 * graphical user interface.
 */
public class MakroPush extends UiApplication
{
	private LoginScreen loginScreen;
	private Main mainScreen;
    /**
     * Entry point for application
     * @param args Command line arguments (not used)
     */ 
    public static void main(String[] args)
    {
        // Create a new instance of the application and make the currently
        // running thread the application's event dispatch thread.
        MakroPush theApp = new MakroPush();       
        theApp.enterEventDispatcher();
    }
    
    public void backToLogin(){
    	this.invokeLater(new Runnable() {
			
			public void run() {
		    	popScreen(mainScreen);
		    	pushScreen(loginScreen);				
			}
		});
    }
    
    public void goToMain(){
    	this.invokeLater(new Runnable() {
			
			public void run() {
				popScreen(loginScreen);
		    	pushScreen(mainScreen);
	            mainScreen.initMessagingPublisher();
			}
		});
    }

    /**
     * Creates a new MakroPush object
     */
    public MakroPush()
    {        
    	mainScreen = new  Main();
    	loginScreen = new LoginScreen();
    	
    	if(User.getInstance().isLogged()){
            pushScreen(mainScreen);
            mainScreen.initMessagingPublisher();
    	}else{
    		pushScreen(loginScreen);
    	}
    }    
}