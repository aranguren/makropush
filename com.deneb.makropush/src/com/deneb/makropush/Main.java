package com.deneb.makropush;

import com.deneb.makropush.model.PushService;
import com.deneb.makropush.model.PushService.OnMessageListener;
import com.deneb.makropush.model.User;

import net.rim.device.api.system.Application;
import net.rim.device.api.system.LED;
import net.rim.device.api.ui.Font;
import net.rim.device.api.ui.MenuItem;
import net.rim.device.api.ui.component.Dialog;
import net.rim.device.api.ui.component.LabelField;
import net.rim.device.api.ui.component.Menu;
import net.rim.device.api.ui.component.table.SimpleList;
import net.rim.device.api.ui.container.MainScreen;
import net.rim.device.api.ui.container.VerticalFieldManager;

/**
 * A class extending the MainScreen class, which provides default standard
 * behavior for BlackBerry GUI applications.
 */
public final class Main extends MainScreen
{
	private MenuItem disconnectMenu = new MenuItem("Desconectar", 110, 10)
	{
		public void run(){
			if(Dialog.ask(Dialog.D_YES_NO, "Deseas salir del sistema de mensajeria?") == Dialog.YES){
				User user = User.getInstance();
				MakroPush app = (MakroPush)Application.getApplication();
				user.clean();
				app.backToLogin();
			}
		}
	};
	
	private MenuItem closeMenu = new MenuItem("Cerrar", 400, 20)
	{
		public void run(){
			Application.getApplication().requestBackground();
			
		}
	};

	private SimpleList messageList;	
    /**
     * Creates a new Main object
     */ 
    public Main()
    {        
        // Set the displayed title of the screen       
        setTitle("Makro Push");
        
        LabelField title = new LabelField("Mensajes Recibidos", LabelField.FIELD_HCENTER);
        title.setFont(Font.getDefault().derive(Font.BOLD, 30));
		title.setMargin(10, 0, 20, 0); //To leave some space from top and bottom
        
		add(title);
        VerticalFieldManager manager = new VerticalFieldManager();
         
        messageList = new SimpleList(manager);
        manager.setMargin(10, 10, 10, 10);
        
        add(manager);
    }
    
    protected void makeMenu(Menu menu, int instance) {
    	 menu.add(disconnectMenu);
         menu.add(closeMenu);
    }
    
    public boolean onClose() {
		Application.getApplication().requestBackground();
		return false;
	};
    
    protected void onFocusNotify(boolean focus) {
    	// TODO Auto-generated method stub;
    	super.onFocusNotify(focus);
        LED.setState(LED.STATE_OFF);
    }
    
    public void initMessagingPublisher(){
    	final Application app = Application.getApplication();
        PushService pushService = PushService.getInstance();
        
        pushService.connect(User.getInstance(), new OnMessageListener() {
			
			public void onMessage(final String from, final String body) {
				app.invokeLater(new Runnable() {
					
					public void run() {
						StringBuffer segments = new StringBuffer(body);
						
						if(segments.length() > 15)
							segments.insert(15, '\n');
						
						messageList.add(segments.toString());
						
						if(!app.isForeground()){
					    	LED.setConfiguration(500, 250, LED.BRIGHTNESS_50);
					    	LED.setState(LED.STATE_BLINKING);
						}
					}
				});
			}
		});
    }
}
